import { useContext, useEffect } from 'react';
import { PageContext } from '../../store/page-context';
import { useDispatch, useSelector } from 'react-redux';
import { countTime } from '../../store/phoneSlice';
import { timerCount } from '../../store/thunks';
import CallingButton from '../UI/CallingButton';
import './Calling.css';

const Calling = (props) => {
	const pageContext = useContext(PageContext);
	const duration = useSelector((state) => state.phoneReducer.duration);
	const isCount = useSelector((state) => state.phoneReducer.isCount);
	const dispatch = useDispatch();

	useEffect(() => {
		if (isCount) {
			dispatch(timerCount(countTime));
		}
	}, [isCount, dispatch]);

	return (
		<div className='dial__calling'>
			<h2 className='dial__calling-brand'>Gcalls</h2>
			<h2 className='dial__calling-recipient'>{pageContext.phoneNumber}</h2>
			<h2 className='dial__calling-status'>
				{pageContext.error
					? pageContext.error
					: isCount
					? duration
					: 'Calling.....'}
			</h2>
			<div className='dial__calling-container'>
				<CallingButton>
					<i className='fas fa-microphone-slash'></i>
					<p>mute</p>
				</CallingButton>
				<CallingButton>
					<i className='fas fa-pause'></i>
					<p>pause</p>
				</CallingButton>
				<CallingButton>
					<i className='fas fa-phone'></i>
					<p>forward</p>
				</CallingButton>
			</div>
		</div>
	);
};

export default Calling;
