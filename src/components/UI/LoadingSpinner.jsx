import styles from './LoadingSpinner.module.css';

const LoadingSpinner = () => {
	return (
		<div className={styles.ring}>
			Loading <span></span>
		</div>
	);
};

export default LoadingSpinner;
