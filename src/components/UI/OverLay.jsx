import styles from './Overlay.module.css';
import LoadingSpinner from './LoadingSpinner';

const Overlay = () => {
	return (
		<div className={styles.overlay}>
			<LoadingSpinner />
		</div>
	);
};

export default Overlay;
