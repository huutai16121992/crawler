import { useContext, useEffect, useState } from 'react';
import JsSIP from 'jssip';
import styles from './TableData.module.css';
import { PageContext } from '../../store/page-context';
import { useDispatch } from 'react-redux';
import { time } from '../../store/thunks';
import {
	startCount,
	stopCount,
	resetTimer,
	countTime,
} from '../../store/phoneSlice';

let callId = '';
const TableData = (props) => {
	const pageContext = useContext(PageContext);
	const [endPoint, setEndPoint] = useState('http://localhost:8080');
	const dispatch = useDispatch();

	useEffect(() => {
		if (process.env.NODE_ENV === 'production') {
			setEndPoint('https://huutai.gcalls.vn');
		}

		if (pageContext.callSession) {
			pageContext.callSession.connection.addEventListener('addstream', (e) => {
				const audio = document.createElement('audio');
				audio.srcObject = e.stream;
				audio.play();
			});
		}
	}, [pageContext]);

	const createContact = async () => {
		try {
			const response = await fetch(`${endPoint}/contact/phone/${props.phone}`);
			const contact = await response.json();
			if (!contact.status) {
				const contactData = {
					name: props.name,
					domain: `http://${props.domain}`,
					address: props.address,
					phone: props.phone,
				};
				const newContactOption = {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(contactData),
				};
				const res = await fetch(`${endPoint}/contact`, newContactOption);
				if (!res.ok) {
					console.log('Fail to create new contact');
					return;
				}
			}
		} catch (error) {
			console.log(error.message);
		}
	};

	const eventHandlers = {
		progress: async (e) => {
			const callData = {
				caller: e.response.from.uri.user,
				recipient: e.response.to._uri.user,
				status: 'makingCall_start',
			};
			const newCallOption = {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(callData),
			};
			const response = await fetch(`${endPoint}/log`, newCallOption);
			if (!response.ok) {
				pageContext.changeError('Fail to connect to server');
				return;
			}
			const data = await response.json();
			callId = data._id;
		},
		failed: async (e) => {
			pageContext.changeError(e.cause);
			pageContext.removeUA();
			pageContext.removeCallSession();
			clearInterval(time);
			const dataUpdate = {
				status: e.cause,
			};
			const updateCallOption = {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(dataUpdate),
			};
			const response = await fetch(
				`${endPoint}/log/${callId}`,
				updateCallOption,
			);
			if (!response.ok) {
				pageContext.changeError('Fail to connect to server');
			}
		},
		ended: async (e) => {
			pageContext.removeUA();
			pageContext.removeCallSession();
			clearInterval(time);
			const dataUpdate = {
				endedBy: props.phone,
			};
			const updateCallOption = {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(dataUpdate),
			};
			const response = await fetch(
				`${endPoint}/log/${callId}`,
				updateCallOption,
			);
			if (!response.ok) {
				pageContext.changeError('Fail to connect to server');
			}
			await createContact();
		},
		confirmed: async (e) => {
			dispatch(startCount());
			dispatch(countTime());
			const dataUpdate = {
				status: 'makingCall_connected',
			};
			const updateCallOption = {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(dataUpdate),
			};
			const response = await fetch(
				`${endPoint}/log/${callId}`,
				updateCallOption,
			);
			if (!response.ok) {
				pageContext.changeError('Fail to connect to server');
			}
		},
	};

	const handleCall = async () => {
		const response = await fetch(`${endPoint}/contact/name/${props.name}`);
		const contact = await response.json();
		if (contact.status) {
			pageContext.changePhoneNumber(contact.contactFound.name);
		} else {
			pageContext.changePhoneNumber(props.phone);
		}
		pageContext.turnOnCalling();
		pageContext.changeError(undefined);
		dispatch(resetTimer());
		dispatch(stopCount());

		const socket = new JsSIP.WebSocketInterface('wss://sbc03.tel4vn.com:7444');
		const configuration = {
			sockets: [socket],
			uri: '105@2-test1.gcalls.vn:50061',
			password: 'test1105',
			session_timers: false,
		};

		const ua = new JsSIP.UA(configuration);
		pageContext.setUa(ua);

		ua.start();

		const options = {
			eventHandlers: eventHandlers,
			mediaConstraints: { audio: true, video: false },
		};

		const session = ua.call(`${'0907074708'}`, options);
		// const session = ua.call(`${props.phone}`, options);
		pageContext.changeCallSession(session);
	};

	return (
		<tr>
			<td>{props.id}</td>
			<td>{props.name}</td>
			<td>{props.address}</td>
			<td>
				{props.phone}{' '}
				<i onClick={handleCall} className={`fas fa-phone ${styles.phone}`}></i>
			</td>
			<td>
				<a className={styles.link} href={`http://${props.domain}`}>
					{props.domain}
				</a>
			</td>
			<td>
				{props.isAlive ? (
					<i className={`fas fa-check-circle ${styles.alive}`}></i>
				) : (
					<i className={`fas fa-times ${styles.dead}`}></i>
				)}
			</td>
		</tr>
	);
};

export default TableData;
