import { useContext, useEffect, useState, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { PageContext } from '../../store/page-context';
import styles from './Table.module.css';
import TableData from '../TableData/TableData';
import Overlay from '../UI/OverLay';

const Table = (props) => {
	const [companyInfo, setCompanyInfo] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(undefined);
	const pageContext = useContext(PageContext);

	useEffect(() => {
		const fetchCompanyInfo = async () => {
			try {
				setIsLoading(true);
				const response = await fetch(pageContext.url);
				if (!response.ok) throw new Error('Have Error when fetching data');
				const result = await response.json();
				setCompanyInfo(result.infor);
				setIsLoading(false);
			} catch (error) {
				setError(error.message);
			}
		};
		fetchCompanyInfo();
	}, [pageContext]);

	if (error) {
		return <h2>{error}</h2>;
	}

	const tableData = companyInfo.map((company) => (
		<TableData
			key={company._id}
			id={company._id}
			name={company.name}
			address={company.address}
			phone={company.teleNumber}
			domain={company.domain}
			logo={company.logo}
			isAlive={company.extensionId.alive}
		/>
	));

	return (
		<Fragment>
			<table className={styles.table}>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Address</th>
						<th>Phone</th>
						<th>Domain</th>
						<th>Is Alive</th>
					</tr>
				</thead>
				<tbody>{tableData}</tbody>
			</table>
			{ReactDOM.createPortal(
				isLoading && <Overlay />,
				document.getElementById('overlay'),
			)}
		</Fragment>
	);
};

export default Table;
