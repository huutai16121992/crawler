import { useState, useEffect, useContext } from 'react';
import ReactPaginate from 'react-paginate';
import { PageContext } from '../../store/page-context';

const Pagination = (props) => {
	const [pageCount, setPageCount] = useState(0);
	const pageContext = useContext(PageContext);

	useEffect(() => {
		const getTotalPage = async () => {
			const response = await fetch(
				'https://crawl.gcall.vn/api/company/totalpage',
			);
			const totalPage = await response.json();
			setPageCount(totalPage.totalPage);
		};
		setPageCount(Math.ceil(pageCount / 20));
		getTotalPage().catch((error) => console.log(error));
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// Invoke when user click to request another page.
	const handlePageClick = (event) => {
		const page = event.selected + 1;
		pageContext.changePage(page);
	};
	return (
		<ReactPaginate
			nextLabel='next >'
			onPageChange={handlePageClick}
			pageRangeDisplayed={3}
			marginPagesDisplayed={2}
			pageCount={pageCount}
			previousLabel='< previous'
			pageClassName='page-item'
			pageLinkClassName='page-link'
			previousClassName='page-item'
			previousLinkClassName='page-link'
			nextClassName='page-item'
			nextLinkClassName='page-link'
			breakLabel='...'
			breakClassName='page-item'
			breakLinkClassName='page-link'
			containerClassName='pagination'
			activeClassName='active'
			renderOnZeroPageCount={null}
		/>
	);
};

export default Pagination;
