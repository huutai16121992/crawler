import styles from './Popup.module.css';
import CancelButton from '../UI/CancelButton';
import Calling from '../Call/Calling';
import { useContext } from 'react';
import { PageContext } from '../../store/page-context';

const Popup = (props) => {
	const pageContext = useContext(PageContext);

	const handleCancel = () => {
		pageContext.turnOffCalling();
		if (pageContext.ua) {
			pageContext.ua.stop();
		}
		pageContext.removeUA();
		pageContext.removeCallSession();
	};

	return (
		<div className={styles.popup}>
			<Calling />
			<CancelButton onClick={handleCancel} />
		</div>
	);
};

export default Popup;
