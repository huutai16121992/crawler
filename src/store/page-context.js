import React, { useState } from 'react';

const endPoint = `https://crawl.gcall.vn/api/company/getdata?_page=`;

const initialState = {
	url: '',
	changePage: (page) => {},
	isCalling: false,
	turnOnCalling: () => {},
	turnOffCalling: () => {},
	phoneNumber: '',
	changePhoneNumber: () => {},
	error: undefined,
	changeError: (error) => {},
	callSession: undefined,
	changeCallSession: (session) => {},
	removeCallSession: () => {},
	ua: undefined,
	setUa: (socket) => {},
	removeUA: () => {},
	callId: '',
	changeCallId: (callId) => {},
};
export const PageContext = React.createContext(initialState);

const ContextProvider = (props) => {
	const [url, setUrl] = useState(endPoint + 1);
	const [isCalling, setIsCalling] = useState(false);
	const [phoneNumber, setPhoneNumber] = useState('');
	const [error, setError] = useState(undefined);
	const [callSession, setCallSession] = useState(undefined);
	const [ua, setNewUA] = useState(undefined);
	const [callId, setCallId] = useState('');

	const changePage = (page) => setUrl(endPoint + page);
	const turnOnCalling = () => setIsCalling(true);
	const turnOffCalling = () => setIsCalling(false);
	const changePhoneNumber = (phone) => setPhoneNumber(phone);
	const changeError = (error) => setError(error);
	const changeCallSession = (session) => setCallSession(session);
	const removeCallSession = () => setCallSession(undefined);
	const setUa = (ua) => setNewUA(ua);
	const removeUA = () => setNewUA(undefined);
	const changeCallId = (id) => setCallId(id);

	const value = {
		url,
		changePage,
		isCalling,
		turnOnCalling,
		turnOffCalling,
		phoneNumber,
		changePhoneNumber,
		error,
		changeError,
		callSession,
		changeCallSession,
		removeCallSession,
		ua,
		setUa,
		removeUA,
		callId,
		changeCallId,
	};

	return (
		<PageContext.Provider value={value}>{props.children}</PageContext.Provider>
	);
};

export default ContextProvider;
