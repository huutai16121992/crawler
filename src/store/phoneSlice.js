import { createSlice } from '@reduxjs/toolkit';

const convertTime = (time) => `0${time}`.slice(-2);

const initialState = {
	seconds: 0,
	minutes: 0,
	duration: '00 : 00',
	isCount: false,
};

const phoneSlice = createSlice({
	name: 'Phone',
	initialState,
	reducers: {
		countTime: (state) => {
			if (state.seconds === 60) {
				state.seconds = 0;
				state.minutes += 1;
			}
			state.seconds += 1;
			state.duration = `${convertTime(state.minutes)} : ${convertTime(
				state.seconds,
			)}`;
		},
		resetTimer: (state) => {
			state.seconds = 0;
			state.minutes = 0;
			state.duration = '00 : 00';
		},
		startCount: (state) => {
			state.isCount = true;
		},
		stopCount: (state) => {
			state.isCount = false;
		},
	},
});

export const { countTime, resetTimer, startCount, stopCount } =
	phoneSlice.actions;

export default phoneSlice.reducer;
