import { useContext } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Pagination from './components/Pagination/Pagination';
import Popup from './components/Popup/Popup';
import TableData from './components/Table/Table';
import { PageContext } from './store/page-context';

function App() {
	const pageContext = useContext(PageContext);

	return (
		<div className='App'>
			<TableData />
			<Pagination className='pagination' />
			{ReactDOM.createPortal(
				pageContext.isCalling && <Popup />,
				document.getElementById('popup'),
			)}
		</div>
	);
}

export default App;
